import { useState } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Pressable,
  Button
} from "react-native";

const Dropdown = (props) => {
  const { status, index, changeStatus, list } = props;
  const [newstatus, setStatus] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View
      style={{
        marginRight: 10,
      }}
    >
      <TouchableOpacity
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          borderWidth: 1,
          borderColor: "black",
          borderRadius: 50,
          padding: 2,
        }}
        onPress={() => {
          setModalVisible(true);
        }}
      >
        <Text
          style={{
            fontSize: 9,
            width: 50,
            textAlign: "center",
            color: list[index].statusCOlor,
            fontWeight: 900,
          }}
        >
          {list[index].status}
        </Text>
        <Image
          style={{
            width: 20,
            height: 20,
          }}
          source={require("../assets/dropdown.png")}
        ></Image>
      </TouchableOpacity>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text
              style={{
                fontSize: 20,
                width: 200,
              }}
            >
              Select Status
            </Text>
            <TouchableOpacity
              onPress={() => {
                changeStatus(index, "Open", "#1aa7ec");
                setModalVisible(false);
              }}
              style={styles.options}
            >
              <View
                style={{
                  backgroundColor: "#1aa7ec",
                  width: 10,
                  height: 10,
                  borderRadius: 50,
                }}
              ></View>
              <Text>Open</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.options}
              onPress={() => {
                changeStatus(index, "In Progress", "#fcd12a");
                setModalVisible(false);
              }}
            >
              <View
                style={{
                  backgroundColor: "#fcd12a",
                  width: 10,
                  height: 10,
                  borderRadius: 50,
                }}
              ></View>
              <Text>In Progress</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.options}
              onPress={() => {
                changeStatus(index, "Done", "#68bb59");
                setModalVisible(false);
              }}
            >
              <View
                style={{
                  backgroundColor: "#68bb59",
                  width: 10,
                  height: 10,
                  borderRadius: 50,
                }}
              ></View>
              <Text>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity
              color="#FcbA00"
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
              title='cancel'
            >
              <Text style={styles.textStyle}>cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    //   alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    marginTop: 20,
    backgroundColor: "#FcbA00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  options: {
    display: "flex",
    flexDirection: "row",
    // justifyContent:'center',
    alignItems: "center",
    gap: 20,
    margin: 10,
  },
});

export default Dropdown;
