import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import Modal from "react-native-modal";
import CheckBox from "react-native-check-box";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Dropdown from "./Dropdown";
import AddModal from "./AddModal";
import { useFocusEffect } from "@react-navigation/native";

const Todo = ({ route }) => {
  const type = route.params.type;

  const [list, setList] = useState([]);
  const [addTodoModal, setAddTodoModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [text, setText] = useState("");
  const [editText, setEditText] = useState("");
  const [editIndex, setEditIndex] = useState(0);
  const toggleModal = () => {
    setAddTodoModal(addTodoModal === false);
    setText("");
  };
  const add = () => {
    setAddTodoModal(addTodoModal === false);
    setList((prev) => {
      if (prev) {
        prev.push({
          text,
          completed: false,
          status: "Open",
          statusCOlor: "#1aa7ec",
        });
      } else {
        prev = [
          {
            text,
            completed: false,
            status: "Open",
            statusCOlor: "#1aa7ec",
          },
        ];
      }
      return prev;
    });
    setText("");
    setTimeout(() => {
      storeData();
    });
  };
  const addEdited = () => {
    console.log(editIndex);
    list[editIndex].text = editText;
    setList(list);
    setEditText("");
    storeData();
    setEditModal(false);
  };
  const deleteItem = (index) => {
    setList((prev) => {
      prev = prev.map((elem, ind) => {
        if (ind === index) {
          elem.status = "closed";
          elem.statusCOlor = "#595959";
          return elem;
        } else {
          return elem;
        }
      });
      return prev;
    });
    setTimeout(() => {
      storeData();
    });
  };
  const reopenItem = (index) => {
    setList((prev) => {
      prev = prev.map((elem, ind) => {
        if (ind === index) {
          elem.status = "Open";
          elem.statusCOlor = "#1aa7ec";
          return elem;
        } else {
          return elem;
        }
      });
      return prev;
    });
  };
  const storeData = async () => {
    try {
      await AsyncStorage.setItem("todo", JSON.stringify(list));
      console.log("Data saved successfully");
    } catch (error) {
      console.error("Error saving data:", error);
    }
  };
  const retrieveData = async () => {
    try {
      let value = await AsyncStorage.getItem("todo");
      value = JSON.parse(value);
      setList(value);
      if (value !== null) {
        console.log("Retrieved data:", value);
      } else {
        console.log("Data not found");
      }
    } catch (error) {
      console.error("Error retrieving data:", error);
      setList([]);
    }
  };
  const changeStatus = (index, value, color) => {
    setList((prev) => {
      prev = prev.map((elem, ind) => {
        if (ind === index) {
          elem.status = value;
          elem.statusCOlor = color;
          return elem;
        } else {
          return elem;
        }
      });
      return prev;
    });
    
  };
  useEffect(() => {
    console.log(type);
    retrieveData();
  }, []);

  useEffect(() => {
    console.log("list changed");
    storeData()
  }, [list]);
  const setTextfromModal = (value) => {
    setText(value)
  }
  const setEditTextfromModal = (value) => {
    setEditText(value)
  }
  const ToggleEditModal = () =>{
    setEditModal(false)
  }
//   useEffect(()=>{
//     console.log("type changed");
//     retrieveData()
//   },[type])

  return (
    <View style={styles.whole}>
      <View>
        <Text
          style={{
            marginBottom: 30,
            textAlign: "center",
            fontSize: 30,
            height: 100,
          }}
        >
        {type} Check List <Image source={require("../assets/check.png")}></Image>
        </Text>
        {list
          ? list.map((elem, index) => {
              if (elem.status===type || type==='All') {
                return (
                  <View key={index} style={styles.container}>
                    <Dropdown
                      status={elem.status}
                      index={index}
                      changeStatus={changeStatus}
                      list={list}
                    />

                    <Text
                      style={
                        elem.status === "closed"
                          ? styles.strikedLine
                          : styles.line
                      }
                    >
                      {elem.text}
                    </Text>
                    {elem.status !== "closed" ? (
                      <View
                        style={{
                          display: "flex",
                          flexDirection: "row",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            setEditModal(!editModal);
                            setEditText(elem.text);
                            setEditIndex(index);
                          }}
                        >
                          <Image
                            source={require("../assets/edit.png")}
                            style={{
                              width: 28,
                              height: 28,
                            }}
                          ></Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                          // style={styles.editButton}
                          onPress={() => {
                            deleteItem(index);
                          }}
                        >
                          <Image
                            source={require("../assets/delete.png")}
                            style={{
                              width: 28,
                              height: 28,
                            }}
                          ></Image>
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <View>
                        <TouchableOpacity
                          style={{
                            borderWidth: 1,
                            borderColor: "black",
                            borderRadius: 50,
                            padding: 4,
                          }}
                          onPress={() => {
                            reopenItem(index);
                          }}
                        >
                          <Text>Reopen</Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                );
              }else{
                return null
              }
            })
          : null}
      </View>
      <AddModal headding="Edit Task" addTodoModal={editModal} toggleModal={ToggleEditModal} add={addEdited} text={editText} setTextfromModal={setEditTextfromModal}/>
      <AddModal headding={'Add new task'} addTodoModal={addTodoModal} toggleModal={toggleModal} add={add} text={text} setTextfromModal={setTextfromModal}/>
      <TouchableOpacity
        style={styles.addButton}
        // title="+ Add Todo"
        // color="#ef4444"
        onPress={toggleModal}
      >
        <Text style={styles.addButtonText}>+ ADD TASK</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Todo;

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#EF4444",
  },
  editButton: {
    height: 28,
    wifth: 28,
    backgroundColor: "blue",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  whole: {
    // backgroundColor:'red',
    height: "100%",
  },
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    // backgroundColor: "#fff",
    marginBottom: 20,
  },
  line: {
    width: "50%",
  },
  strikedLine: {
    width: "50%",
    textDecorationLine: "line-through",
  },
  addButton: {
    height: 50,
    borderRadius: 25,
    backgroundColor: "#FcbA00",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: 20,
    marginBottom: 10,
    position: "absolute",
    bottom: 16,
    width: "90%",
  },
  addButtonText: {
    fontSize: 18,
    // fontWeight: 'bold',
    color: "#1c1c1c",
  },
  modal: {
    backgroundColor: "red",
    // height:'50%'
  },
  input: {
    borderBlockColor: "black",
    borderWidth: 1,
    padding: 10,
  },
});
