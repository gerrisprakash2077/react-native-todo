import React, { useState } from 'react'
import {View, Text, Button} from 'react-native'

export default function Hello(props) {
    const{first,second} = props
    const [num,setNum]=useState(0);
  return (
    <View>
      <Text>{first}</Text>
      <Text>{second}</Text>
      <Text>{num}</Text>
      <Button title='increment' onPress={()=>{
        setNum(num+1)
      }}></Button>
    </View>
  )
}
