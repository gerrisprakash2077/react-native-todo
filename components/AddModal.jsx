import React from "react";
import { useState } from "react";
import {
  Modal,
  View,
  TextInput,
  StyleSheet,
  Button,
  Text,
  TouchableOpacity,
} from "react-native";

const AddModal = (props) => {
  const { headding, addTodoModal, toggleModal, add, text, setTextfromModal } =
    props;
  const [hasFocus, setHasFocus] = useState(false);
  return (
    <Modal
      style={{
        backgroundColor: "red",
        height: 500,
      }}
      animationType="fade"
      visible={addTodoModal}
      transparent={true}
    >
      <View style={{
        // backgroundColor:'red',
        height:'100%',
        backgroundColor: 'rgba(0, 0, 0, 0.7)'
      }}>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            height: 300,
            backgroundColor: "#fff",
            borderRadius: 30,
            padding: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 200,
              height: 200,
            },
            shadowOpacity: 1,
            // shadowRadius: 4,
            elevation: 50,
            // elevation:200
            marginTop: 200,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              marginBottom: 30,
            }}
          >
            {headding}
          </Text>
          <TextInput
            autoFocus={true}
            // style={styles.input}
            onChangeText={(value) => {
              setTextfromModal(value);
            }}
            style={hasFocus ? styles.focusedTextInput : styles.input}
            value={text}
            placeholder="Enter New Task"
            onFocus={() => {
              setHasFocus(true);
            }}
            onBlur={() => {
              setHasFocus(false);
            }}
          ></TextInput>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              gap: 10,
              margin: 10,
            }}
          >
            <TouchableOpacity
              style={{
                marginTop: 20,
                borderRadius: 50,
                // backgroundColor:'#ddd',
                padding: 10,
                // borderBlockColor:'#ef4444',
                paddingLeft: 20,
                paddingRight: 20,
                borderWidth: 1.5,
                borderColor: "#1c1c1c",
              }}
              color="#ef4444"
              title="cancel"
              onPress={toggleModal}
            >
              <Text
                style={{
                  color: "#1c1c1c",
                  fontWeight: 500,
                }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginTop: 20,
                borderRadius: 50,
                backgroundColor: "#FcbA00",
                padding: 10,
                paddingLeft: 20,
                paddingRight: 20,
              }}
              title="confirm"
              color="#ef4444"
              disabled={text == ""}
              onPress={add}
            >
              <Text
                style={{
                  color: "#1c1c1",
                  fontWeight: 500,
                }}
              >
                Confirm
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};
export default AddModal;

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#EF4444",
  },
  editButton: {
    height: 28,
    wifth: 28,
    backgroundColor: "blue",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  whole: {
    // backgroundColor:'red',
    height: "100%",
  },
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    // backgroundColor: "#fff",
    marginBottom: 20,
  },
  line: {
    width: "50%",
  },
  strikedLine: {
    width: "50%",
    textDecorationLine: "line-through",
  },
  addButton: {
    height: 50,
    borderRadius: 25,
    backgroundColor: "red",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: 20,
    marginBottom: 10,
    position: "absolute",
    bottom: 16,
    width: "90%",
  },
  addButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
  },
  modal: {
    backgroundColor: "red",
    // height:'50%'
  },
  input: {
    borderRadius: 3,
    borderBlockColor: "black",
    borderWidth: 1,
    padding: 10,
  },
  focusedTextInput: {
    borderRadius: 3,
    borderBlockColor: "#719ECE",
    borderWidth: 2,
    padding: 10,
    // backgroundColor:'red',
    borderColor: "blue",
  },
});
