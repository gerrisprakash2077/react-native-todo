import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Hello from './components/Hello'
import Todo from './components/Todo'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useState } from 'react';

const Tab = createBottomTabNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Check List" component={Todo}
        options={{
          tabBarLabel: 'All',
          tabBarIcon: ({ color, size }) => (
            <Text>|||</Text>
          ),
        }} 
        initialParams={{
          type: 'All',
        }}
        />
        {/* <Tab.Screen name="Open" component={Todo}
        options={{
          tabBarLabel: 'Open',
          tabBarIcon: ({ color, size }) => (
            <Text>|||</Text>
          ),
        }} 
        initialParams={{
          type: 'Open',
        }}
        />
        <Tab.Screen name="In Progress" component={Todo} 
        options={{
          tabBarLabel: 'In Progress',
          tabBarIcon: ({ color, size }) => (
            <Text>|||</Text>
          ),
        }}
        initialParams={{
          type: 'In Progress',
        }}/>
        <Tab.Screen name="Done" component={Todo} 
        options={{
          tabBarLabel: 'Done',
          tabBarIcon: ({ color, size }) => (
            <Text>|||</Text>
          )
        }}
        initialParams={{
          type: 'Done',
        }}/>
        <Tab.Screen name="Closed" component={Todo}
        options={{
          tabBarLabel: 'Closed',
          tabBarIcon: ({ color, size }) => (
            <Text>|||</Text>
          ),
        }}
        initialParams={{
          type: 'closed',
        }}
        /> */}
        
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
